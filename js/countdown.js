const getTime = dateTo => {
    console.log('Tiempos ' + dateTo );
    let now = new Date(),
        time = (new Date(dateTo) - now + 1000) / 1000,
        seconds = ('0' + Math.floor(time % 60)).slice(-2),
        minutes = ('0' + Math.floor(time / 60 % 60)).slice(-2),
        hours = ('0' + Math.floor(time / 3600 % 24)).slice(-2);
        days = Math.floor(time / (3600 * 24));

    
    return {
        seconds,
        minutes,
        hours,
        days,
        time
    }
};

const countdown = (dateTo, element) => {
 
    const item = document.getElementById(element);
    const timerUpdate = setInterval( () => {
        let currenTime = getTime(dateTo);
        item.innerHTML = `
            <div class="row justify-content-center">
            
                <div class="col-6 col-lg-4">
                    <div class="countdown-container fazul" id="divHoras">
                        <div class="number">
                            ${currenTime.hours}
                        </div>
                        <div class="concept">
                            Horas
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="countdown-container frojo" id="divMinutos">
                        ${currenTime.minutes}
                        <div class="concept">
                            Minutos
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="countdown-container fverde" id="divSegundos">
                       
                            ${currenTime.seconds}
                        
                        <div class="concept">
                            Segundos
                        </div>
                    </div>
                </div>
            </div>`;

        if (currenTime.time <= 1) {
            clearInterval(timerUpdate);
            console.log('Fin de la cuenta '+ element);
            
        }

    }, 1000);
};

// obtenemos la fecha actual
var date = new Date()
var new_date1 = new Date(date);

// Obtenemos un numero aleatorio entre 1 y 12
new_date1.setHours(20,0,0);
// Incrementamos las horas
//new_date1.setHours(2);
// Incrementamos los dias
new_date1.setDate(4);
// Incrementamos los meses
new_date1.setMonth(2);



document.getElementById('new_date1').innerHTML = new_date1; 


countdown(new_date1, 'countdown1');
